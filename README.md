Download:
```bash
php composer.phar create-project paragor/console-skeleton
```
Build phar:
```bash
./vendor/bin/phing
```
Run phar:
```bash
./build/artifacts/console.phar
```
Rename project name in `build.xml:2` to rename artifact
