<?php


namespace App\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Hello extends Command
{
    public function configure()
    {
        $this
            ->setName('hello');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('work');
    }
}