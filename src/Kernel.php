<?php

namespace App;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\TaggedContainerInterface;

class Kernel
{
    /**
     * @var string
     */
    private $environment;

    /**
     * @var TaggedContainerInterface
     */
    private $container;

    /**
     * Kernel constructor.
     * @param string $environment
     *
     * @throws \Exception
     */
    public function __construct(string $environment)
    {
        $this->environment = $environment;
        $this->initContainer();
    }

    /**
     * @return void
     *
     * @throws \Exception
     */
    protected function initContainer()
    {
        $rootDir = $this->getRootDir();
        $container = new ContainerBuilder();
        $container->setParameter('kernel.dir', $rootDir);
        $this->loadContainerConfig($container);
        $this->container = $container;
    }

    /**
     * @return string
     */
    public function getRootDir(): string
    {
        return __DIR__;
    }

    /**
     * @param ContainerBuilder $container
     * @return void
     *
     * @throws \Exception
     */
    protected function loadContainerConfig(ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator($this->getRootDir() . '/../app/config'));
        $loader->load('config.yml');
    }

    /**
     * @return TaggedContainerInterface
     */
    public function getContainer(): TaggedContainerInterface
    {
        return $this->container;
    }

    /**
     * @return string
     */
    public function getEnvironment(): string
    {
        return $this->environment;
    }
}