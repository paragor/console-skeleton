<?php

namespace App;

use App\Command\Hello;
use Symfony\Component\Console\Application as ConsoleApplication;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

class Application extends ConsoleApplication
{
    /**
     * @var Kernel
     */
    private $kernel;

    /**
     * Application constructor.
     * @param Kernel $kernel
     *
     * @throws InvalidArgumentException
     */
    public function __construct(Kernel $kernel)
    {
        $this->kernel = $kernel;

        $container = $kernel->getContainer();

        parent::__construct(
            $container->getParameter('application.name') ??  'UNKNOWN',
            $container->getParameter('application.version') ?? 'UNKNOWN'
        );

        $this->addConsoleCommands();
    }

    /**
     * @return void
     *
     * @throws InvalidArgumentException
     */
    protected function addConsoleCommands()
    {
        $commands = $this->kernel->getContainer()->findTaggedServiceIds('console.command');

        foreach (array_keys($commands) as $command) {
            $this->add($this->kernel->getContainer()->get($command));
        }
    }

    /**
     * @param Command $command
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws \Throwable
     */
    protected function doRunCommand(Command $command, InputInterface $input, OutputInterface $output)
    {
        if ($command instanceof ContainerAwareInterface) {
            $command->setContainer($this->kernel->getContainer());
        }

        return parent::doRunCommand($command, $input, $output);
    }
}