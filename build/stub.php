#!/usr/bin/env php
<?php

try {
    Phar::mapPhar('this.phar');
    include 'phar://this.phar/bin/run.php';
} catch (PharException $e) {
    echo $e->getMessage();
    echo 'Cannot initialize Phar';
    exit(1);
}

__HALT_COMPILER();
