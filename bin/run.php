<?php

require_once __DIR__ . '/../vendor/autoload.php';

(new \App\Application(new \App\Kernel('prod')))->run();
